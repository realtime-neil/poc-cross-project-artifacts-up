# poc-cross-project-artifacts-up/Makefile

all: upstream-variables.yml

upstream-variables.yml: predefined_variables.list
	{ \
		echo '.upstream-variables:' ; \
		echo '  variables:' ; \
		sort | sed 's/.*/    UPSTREAM_\0: $$\0/' ; \
	} <$< >$@

predefined_variables.list: predefined_variables.txt
	awk \
		'/^[ ][ ][\`][[:graph:]]+[\`][[:space:]]+/{ \
			gsub(/^[[:space:]]+/,""); \
			gsub(/[\`]/,""); \
			print($$1); \
		}' \
		<$< >$@

predefined_variables.txt: predefined_variables.md
	pandoc -o $@ $<

predefined_variables.md:
	curl -fsSLo $@ "https://gitlab.com/gitlab-org/gitlab/-/raw/master/doc/ci/variables/predefined_variables.md"
